<?php
class Restaurants extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Restaurants';

		$data['restaurants']=$this->restaurant_model->get_restaurants();

		/*if($this->find()!=NULL) $data['path']='restaurants/view/'.$this->find();
		else{
			$data['path']='restaurants';
			//$this->session->set_flashdata('search_invalid', 'No restaurant with that name.');
		}*/

		if($this->find()!=NULL) $data['restaurant_slug']=$this->find();
		else $data['restaurant_slug']="";

		foreach($data['restaurants'] as $restaurant){
			if($this->user_model->get_user($restaurant['user_id'])) {
				$data['rest'.$restaurant['id']] = $this->user_model->get_user($restaurant['user_id']);
			}
			else $data['rest'.$restaurant['id']] ="";
		}

		$this->load->view('templates/header', $data);
		$this->load->view('restaurants/index', $data);
		$this->load->view('templates/footer', $data);
	}

	public function view($slug= NULL){
		if($this->find()!=NULL) $slug=$this->find();
		$data['restaurant'] = $this->restaurant_model->get_restaurants($slug);


		$restaurant_id = $data['restaurant']['id'];
		$data['comments'] = $this->comment_model->get_comments($restaurant_id);

		if($this->user_model->get_user($data['restaurant']['user_id'])) {
			$data['rest'.$data['restaurant']['id']] = $this->user_model->get_user($data['restaurant']['user_id']);
		}
		else $data['rest'.$data['restaurant']['id']] ="";

		$queryMembers =$this->staff_model->get_members();
		$data['staffMembers'] = $queryMembers;
		$data['categories'] = $this->star_model->get_categories();
		$data['ratings'] = $this->star_model->get_ratings();
		$data['awards'] = $this->star_model->get_awards();

		$i=0;
		foreach ($queryMembers as $member){
			$data['rating_of_'.$member['SID']] = $this->star_model->get_ratings_member($member['SID']);
			//print_r($data['rating_of_'.$member['SID']]);
		}


		if(empty($data['restaurant'])){
			show_404();
		}

		$data['title'] = $data['restaurant']['RName'];

		$this->load->view('templates/header', $data);
		$this->load->view('restaurants/view', $data);
		$this->load->view('templates/footer', $data);
	}

	public function create(){
		if(!$this->session->userdata('logged_in')) redirect('users/login');

		$data['title'] = 'Add new Restaurant';

		$this->form_validation->set_rules('RName', 'RName', 'required');
		$this->form_validation->set_rules('RCity', 'RCity', 'required');

		if($this->form_validation->run() === FALSE){

			$rez = $this->session->userdata('role_id');

			$this->load->view('templates/header', $data);
			$this->load->view('restaurants/create', $data);
			$this->load->view('templates/footer', $data);
		} else{
			//Upload image
			$config['upload_path'] = './restaurant_images';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '2048';
			$config['max_width'] = '2000';
			$config['max_height'] = '2000';

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload()){
				$errors = array('error' => $this->upload->display_errors());
				$restaurant_image = 'noimage.jpg';
			} else {
				$data = array('upload_data' => $this->upload->data());
				$restaurant_image = $_FILES['userfile']['name'];
			}

			$this->session->set_flashdata('restaurant_created', 'Your restaurant is now on Karly\'s Stars. You can now add your staff members.');
			$this->restaurant_model->create_restaurant($restaurant_image);

			redirect('restaurants');
		}
	}

	public function delete($id){
		if(!$this->session->userdata('logged_in')) redirect('users/login');
		$this->restaurant_model->delete_restaurant($id);
		$this->session->set_flashdata('restaurant_deleted', 'Your restaurant has been removed from the list.');
		redirect('restaurants');
	}

	public function edit($slug){
		if(!$this->session->userdata('logged_in')) redirect('users/login');
		$data['restaurant'] = $this->restaurant_model->get_restaurants($slug);

		if(empty($data['restaurant'])){
			show_404();
		}

		$data['title'] = 'Edit restaurant';

		$this->load->view('templates/header', $data);
		$this->load->view('restaurants/edit', $data);
		$this->load->view('templates/footer', $data);
	}

	public function update(){
		if(!$this->session->userdata('logged_in')) redirect('users/login');
		$this->restaurant_model->update_restaurant();
		redirect('restaurants');
	}

	public function find(){
			$restaurant = $this->restaurant_model->find_restaurant();
			$slug = $restaurant['slug'];
			return $slug;
	}

	public function fetch(){
		echo $this->restaurant_model->fetch_data($this->uri->segment(3));
	}

	//STAFF

	public function add_member($id){
		if(!$this->session->userdata('logged_in')) redirect('users/login');
		$data['title'] = 'Add new staff member';
		$data['restaurant'] = $this->restaurant_model->get_restaurant_id($id);


		$this->form_validation->set_rules('SName', 'SName', 'required');
		$this->form_validation->set_rules('SSurname', 'SSurname', 'required');
		$this->form_validation->set_rules('Shift', 'Shift', 'required');

		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('restaurants/add_member', $data);
			$this->load->view('templates/footer', $data);
		} else{
			$this->staff_model->create_member($id);
			$this->session->set_flashdata('member_created', 'Your staff member has been added to the list.');
			redirect('restaurants');
		}
	}

	public function delete_member($id){
		if(!$this->session->userdata('logged_in')) redirect('users/login');
		$slug = $this->staff_model->get_restaurant_by_staff($id);
		$this->staff_model->delete_member($id);
		//$slug = $this->staff_model->get_restaurant_by_staff($id);
		$this->session->set_flashdata('member_deleted', 'Your staff member has been removed from the list.');

		print_r($slug);
		redirect('restaurants/'.$slug);
	}

	//Rating

	public function create_rating($staff_id){
		$slug = $this->staff_model->get_restaurant_by_staff($staff_id);
		$r_id = $this->staff_model->get_restaurant_by_staff_id($staff_id);
		$this->star_model->create($staff_id, $r_id);
		$this->session->set_flashdata('rating_created', 'Your rating has been successful.');
		redirect('restaurants/'.$slug);
	}

	public function give_award($staff_id){
		$slug = $this->staff_model->get_restaurant_by_staff($staff_id);
		$this->star_model->update_award($staff_id);
		redirect('restaurants/'.$slug);
	}

	public function ratings($r_id){
		if(!$this->session->userdata('logged_in')) redirect('users/login');
		if($this->session->userdata('role_id')!=1) redirect('restaurants');
		$data['title'] = 'Ratings';

		$data['restaurants']=$this->restaurant_model->get_restaurants();
		$data['ratings']=$this->star_model->get_all_ratings($r_id);


		$this->load->view('templates/header', $data);
		$this->load->view('restaurants/ratings', $data);
		$this->load->view('templates/footer', $data);
	}

}
