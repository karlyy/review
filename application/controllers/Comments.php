<?php
class Comments extends CI_Controller{



	public function create($restaurant_id){
		$data['title']="Comments";
		$data['restaurant'] = $this->restaurant_model->get_restaurant_id($restaurant_id);
		$data['comments'] = $this->comment_model->get_comments($restaurant_id);

		/*foreach ($data['comments'] as $comment){
			if($this->comment_model->get_creator($comment['id'])) $data['comm'] = $this->comment_model->get_creator($comment['id']);
			else $data['comm'] = 'unknown';
		}*/

		$data['comm'] = $this->comment_model->get_creator('24');

		foreach($data['comments'] as $comment){
			/*if($this->comment_model->get_creator($comment['id'])) {
				$data['c'.$comment['id']] = $this->comment_model->get_creator($comment['id']);
			}
			else $data['c'.$comment['id']] ="unknown";*/
			$data['comment_'.$comment['id']] = $this->comment_model->get_creator($comment['id']);
		}


		$this->form_validation->set_rules('text', 'text', 'required');


		if($this->form_validation->run() === FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('comments/index', $data);
			$this->load->view('templates/footer', $data);
		} else {
			$this->comment_model->create_comment($restaurant_id);
			redirect('restaurants');
		}
	}

	public function delete($id){
		$this->comment_model->delete_comment($id);
		redirect('restaurants');
	}
}
