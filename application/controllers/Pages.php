<?php
class Pages extends CI_Controller {

	public function view($page = 'home')
	{
		if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_404();
		}
		if($this->find()!=NULL) $data['restaurant_slug']=$this->find();
		else $data['restaurant_slug']="";

		$data['title'] = ucfirst($page); // Capitalize the first letter
		$this->load->view('templates/header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	public function find(){
		$restaurant = $this->restaurant_model->find_restaurant();
		$slug = $restaurant['slug'];
		return $slug;
	}
}
