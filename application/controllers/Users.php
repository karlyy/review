<?php
class Users extends CI_Controller{
	public function register(){
		$data['title'] = 'Register';
		$data['roles'] = $this->user_model->get_roles();
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('surname', 'Surame', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|callback_check_username_exists');
		//$this->form_validation->set_rules('role', 'Role', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|callback_check_email_exists');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('password2', 'Confirm Password', 'matches[password]');

		if($this->form_validation->run()===FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('users/register', $data);
			$this->load->view('templates/footer', $data);

		}else{
			//Encrypt password
			$enc_password = md5($this->input->post('password'));
			$this->user_model->register($enc_password);
			$this->session->set_flashdata('user_registered', 'You are now registered and can log in');
			redirect('home');
		}
	}

	public function login(){
		$data['title'] = 'Log in';
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if($this->form_validation->run()===FALSE){
			$this->load->view('templates/header', $data);
			$this->load->view('users/login', $data);
			$this->load->view('templates/footer', $data);

		}else{
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			$array = $this->user_model->login($username, $password);
			if($array){
				$user_data = array(
					'user_id' => $array['id'],
					'username' => $username,
					'logged_in' => true,
					'role_id' => $array['role']
				);
				$this->session->set_userdata($user_data);
				$this->session->set_flashdata('user_logged', 'You are now loged in. Go to restaurants from navbar or click \'Get started\'.');
				redirect('home');
			}
			else{
				$this->session->set_flashdata('login_failed', 'There was a mistake in your input. Try logging in again.');
				redirect('users/login');
			}
		}
	}

	public function logout(){
		$this->session->unset_userdata('logged_in');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('role_id');
		$this->session->unset_userdata('username'); //TODO add role
		$this->session->set_flashdata('login_failed', 'You are logged out.');

		redirect('users/login');
	}

	public function check_username_exists($username){
		$this->form_validation->set_message('check_username_exists', 'That username is taken. Try again.');
		return $this->user_model->check_username_exists($username) ? true : false;
	}

	public function check_email_exists($email){
		$this->form_validation->set_message('logged_out', 'That username is taken. Try again.');
		return $this->user_model->check_email_exists($email) ? true : false;
	}
}
