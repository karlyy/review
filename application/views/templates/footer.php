</div>
<footer >
	<div class="container padding">
		<div class="row text-center">
			<div class="col-md-4">
				<hr class="light">
				<h5>Editor</h5>
				<hr class="light">
				<p>+386 70 174 129</p>
				<p>trajkovskakarolina@gmail.com</p>
				<p>More contact details<br>on <a href="<?php base_url() ?>contact">Contact</a> page.</p>
			</div>
			<div class="col-md-4">
				<hr class="light">
				<h5>Useful links</h5>
				<hr class="light">
				<p><a href="<?php echo base_url(); ?>">Home</a></p>
				<p><a href="<?php echo base_url(); ?>restaurants">Get started</a></p>
				<p><a href="<?php echo base_url(); ?>help">Help</a></p>
				<p><a href="<?php echo base_url(); ?>about">About</a></p>
			</div>
			<div class="col-md-4">
				<img src="<?php echo base_url(); ?>/images/newLogo2.png" width="200">
			</div>
			<div class="col-12">
				<hr class="light-cela">
				<em>&copy; Karly's Stars </em>
			</div>
		</div>
	</div>
</footer>


</body>
</html>
