<?php
   class Restaurant_model extends CI_Model{
   		public function __construct()
		{
			$this->load->database();
		}

		public function get_restaurants($slug=FALSE){
   			if($slug === FALSE){
   				$query = $this->db->get('restaurants');
   				return $query->result_array();
			}
   			$query=$this->db->get_where('restaurants', array('slug'=> $slug));
   			return $query->row_array();
		}

		public function get_restaurant_id($id){
			$query=$this->db->get_where('restaurants', array('id'=> $id));
			return $query->row_array();
		}

		public function find_restaurant(){
			$name= $this->input->post('search');
			$query=$this->db->get_where('restaurants', array('RName'=> $name));
			return $query->row_array();
		}

	   public function fetch_data($query)
	   {
		   $this->db->like('RName', $query);
		   $query = $this->db->get('restaurants');
		   if($query->num_rows() > 0)
		   {
			   foreach($query->result_array() as $row)
			   {
				   $output[] = array(
					   'name'  => $row["RName"],
					   'city'  => $row["RCity"]
				   );
			   }
			   echo json_encode($output);
		   }
	   }

		public function create_restaurant($restaurant_image){
   			$slug = url_title($this->input->post('RName'));

   			$user_id = 0;
   			print_r($this->session->userdata('role_id')===1);
   			print_r("aaaaaaaaa");
			print_r($this->session->userdata('user_id'));
			if($this->session->userdata('role_id')==1) $user_id = $this->session->userdata('user_id');
			print_r($user_id);

   			$data = array(
   				'RName' => $this->input->post('RName'),
				'slug' => $slug,
				'RCity' => $this->input->post('RCity'),
				'restaurant_image' => $restaurant_image,
				'user_id' => $user_id
			);

   			return $this->db->insert('restaurants', $data);
		}

		public function delete_restaurant($id){
   			$this->db->where('id', $id);
   			$this->db->delete('restaurants');
   			return true;
		}

		public function update_restaurant(){
   			//echo $this->input->post('id'); die();
			$slug = url_title($this->input->post('RName'));

			$data = array(
				'RName' => $this->input->post('RName'),
				'slug' => $slug,
				'RCity' => $this->input->post('RCity')
			);
			$this->db->where('id', $this->input->post('id'));
			return $this->db->update('restaurants', $data);
		}
   }
