<?php
class StaffMember_model extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_members($slug=FALSE){
		if($slug === FALSE){
			$query = $this->db->get('staff_members');
			return $query->result_array();
		}
		$query=$this->db->get_where('staff_members', array('slug'=> $slug));
		return $query->row_array();
	}

	public function create_member(){
		$slug = url_title($this->input->post('SName'));

		$data = array(
			'SName' => $this->input->post('SName'),
			'slug' => $slug,
			'SSurname' => $this->input->post('SSurname'),
			'Shift' => $this->input->post('Shift'),
			'RID' => $this->input->post('RID')
		);
		return $this->db->insert('staff_members', $data);
	}

	public function delete_member($id){
		$this->db->where('SID', $id);
		$this->db->delete('staff_members');
		return true;
	}

	public function update_member(){
		//echo $this->input->post('id'); die();
		$slug = url_title($this->input->post('SName'));

		$data = array(
			'RName' => $this->input->post('SName'),
			'slug' => $slug,
			'SSurname' => $this->input->post('SSurname'),
			'Shift' => $this->input->post('Shift'),
			'RID' => $this->input->post('RID')
		);
		$this->db->where('SID', $this->input->post('SID'));
		return $this->db->update('staff_members', $data);
	}
}
