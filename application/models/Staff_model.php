<?php
class Staff_model extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_members($slug=FALSE){
		if($slug === FALSE){
			$this->db->join('restaurants', 'restaurants.id = staff_members.RID' );
			$query = $this->db->get('staff_members');
			return $query->result_array();
		}
		$query=$this->db->get_where('staff_members', array('slug'=> $slug));
		return $query->row_array();
	}

	public function get_restaurant_by_staff($staff_id){
		$this->db->where('SID', $staff_id);

		$result = $this->db->get('staff_members');

		if($result->num_rows()==1){
			print_r($result->row(0)->RID);
			$this->db->where('id', $result->row(0)->RID);
			$res = $this->db->get('restaurants');
			if($res->num_rows()==1) {
				return $res->row(0)->slug;
			}
			else return false;

		}
		else{
			return false;
		}
	}

	public function get_restaurant_by_staff_id($staff_id){
		$this->db->where('SID', $staff_id);

		$result = $this->db->get('staff_members');

		if($result->num_rows()==1){
			print_r($result->row(0)->RID);
			$this->db->where('id', $result->row(0)->RID);
			$res = $this->db->get('restaurants');
			if($res->num_rows()==1) {
				return $res->row(0)->id;
			}
			else return false;

		}
		else{
			return false;
		}
	}

	public function create_member($id){
		$query=$this->db->get_where('restaurants', array('id'=> $id))->row_array();

		$data = array(
			'SName' => $this->input->post('SName'),
			'slug' => $this->input->post('SName'),
			'SSurname' => $this->input->post('SSurname'),
			'Shift' => $this->input->post('Shift'),
			'RID' => $query['id'],
			'user_id' => $this->session->userdata('user_id'),
			'role_id'=>  $this->input->post('role_id'),
			'award_id'=>0
		);
		return $this->db->insert('staff_members', $data);
	}

	public function delete_member($id){
		$this->db->where('SID', $id);
		$this->db->delete('staff_members');
		return true;
	}

}
