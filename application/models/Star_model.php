<?php
class Star_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function get_categories(){
		$query = $this->db->get('categories');
		return $query->result_array();
	}

	public function get_ratings(){
		$query = $this->db->get('ratings');
		return $query->result_array();
	}

	public function get_all_ratings($restaurant_id){
		$queryy = $this->db->query('SELECT*
									FROM ratings_of_member
									JOIN staff_members
									ON ratings_of_member.staff_id = staff_members.SID
									JOIN restaurants
									ON ratings_of_member.restaurant_id = restaurants.id
									JOIN users
									ON ratings_of_member.user_id = users.id
									JOIN ratings
									ON ratings_of_member.rating_id = ratings.id
									JOIN categories
									ON ratings_of_member.category_id= categories.id;');
		$arr = array();
		$i=0;
		foreach ($queryy->result_array() as $q){
			if($q['restaurant_id']==$restaurant_id) {
				$arr[$i] = $q;
			}
			$i++;
		}
		return $arr;
	}

	public function get_awards(){
		$query = $this->db->get('awards');
		return $query->result_array();
	}

	public function get_ratings_member($member_id){
		$query = $this->db->get_where('ratings_of_member', array('staff_id' => $member_id));
		$i=0;
		$arr = array();
		foreach ($query->result_array() as $temp){
			$arr[$i]=$temp['rating_id'];
			$i++;
		}
		if(count($arr)===0) return 0;

		$rating=0;

		foreach ($arr as $a){
			$rating+=$a;
		}

		$rating=$rating/count($arr);

		return $rating;
	}

	public function create($staff_id, $r_id){
		if($this->session->userdata('user_id')!= NULL) $id = $this->session->userdata('user_id');
		else $id = 0;

		$data = array(
			'staff_id' => $staff_id,
			'category_id' => $this->input->post('category'),
			'rating_id' => $this->input->post('rating'),
			'restaurant_id' => $r_id,
			'user_id' => $id
		);
		return $this->db->insert('ratings_of_member', $data);
	}

	public function update_award($staff_id){
		$data = array(
			'SID' => $staff_id,
			'RID' => $this->input->post('RID'),
			'user_id' => $this->input->post('user_id'),
			'SName'=> $this->input->post('SName'),
			'SSurname'=>$this->input->post('SSurname'),
			'Shift'=>$this->input->post('Shift'),
			'slug'=>$this->input->post('slug'),
			'role_id'=>$this->input->post('role_id'),
			'award_id'=>$this->input->post('award')
		);
		print_r("id");
		print_r($staff_id);
		print_r($data);
		$this->db->where('SID', $staff_id);
		return $this->db->update('staff_members', $data);
	}

}
