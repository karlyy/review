<?php
class Comment_model extends CI_Model{
	public function __construct(){
		$this->load->database();
	}

	public function create_comment($restaurant_id){
		if($this->session->userdata('user_id')!= NULL) $id = $this->session->userdata('user_id');
		else $id = 0;
		$data = array(
			'RID' => $restaurant_id,
			'user_id' => $id,
			'text' => $this->input->post('text')
		);

		return $this->db->insert('comments', $data);
	}

	public function get_comments($restaurant_id){
		$this->db->join('restaurants', 'restaurants.id = comments.RID' );
		$query = $this->db->get_where('comments', array('RID' => $restaurant_id));
		return $query->result_array();
	}

	public function delete_comment($id){
		$this->db->where('id', $id);
		$this->db->delete('comments');
		return true;
	}

	public function get_creator($comment_id){
		/*$query = $this->db->get_where('comments', array('id' => $comment_id));
		$arr = $query->row_array();
		$user_id = $arr['user_id'];
		$query = $this->db->get_where('users', array('id' => $comment_id));*/

		$this->db->join('users', 'users.id=comments.user_id' );
		//print_r($this->db->get('comments')->result_array());
		$query = $this->db->get_where('comments', array('comments.id' => $comment_id));
		$arr = $query->row_array();
		//print_r($query->row_array());
		return $arr['name'].' '.$arr['surname'];
	}
}
